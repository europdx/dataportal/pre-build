# Pre-build

[![pipeline status](https://gitlab.ics.muni.cz/europdx/dataportal/pre-build/badges/master/pipeline.svg)](https://gitlab.ics.muni.cz/europdx/dataportal/pre-build/commits/master)

Image to speed up building https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-sourcecode.git is prepared in this repository