FROM debian:stretch

LABEL maintainer="dataportal-edirex@ics.muni.cz"

RUN apt-get update \
    && apt-get install -y openjdk-8-jdk \
    && apt-get install -y git \
    && apt-get install -y maven
    
ADD https://gitlab.ics.muni.cz/api/v4/projects/780/repository/commits commits.json
RUN git clone --depth 1 https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-sourcecode
#COPY . /dataportal-sourcecode

WORKDIR /dataportal-sourcecode
RUN mvn clean package
WORKDIR /dataportal-sourcecode/web/target
RUN rm -rf /dataportal-sourcecode
#CMD ["java","-jar","web-1.0.0.jar"]
